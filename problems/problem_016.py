# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    # check if x and y are between 0 and 10
    if 10 >= x >= 0 and 10 >= y >= 0:
        return True
    # otherwise
    return False

print(is_inside_bounds(9, 5))
print(is_inside_bounds(9, 15))
