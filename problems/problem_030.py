# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    # check if there more than 1 item in the list
    if len(values) > 1:
        # sort the values
        sorted_values = sorted(values)
        # return the second to last value in the list
        return sorted_values[-2]


print(find_second_largest([1, 6, 9, 22, 34, 7]))
